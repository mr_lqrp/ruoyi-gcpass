package com.ruoyi.bigscreen.core.module.manage.service;

import com.ruoyi.dataset.common.vo.PageVO;
import com.ruoyi.bigscreen.core.module.basic.entity.PageEntity;
import com.ruoyi.bigscreen.core.module.basic.service.IBasePageService;
import com.ruoyi.bigscreen.core.module.manage.dto.DataRoomPageDTO;
import com.ruoyi.bigscreen.core.module.manage.dto.DataRoomSearchDTO;

/**
 * @author hongyang
 * @version 1.0
 * @date 2023/3/13 10:59
 */
public interface IDataRoomPageService extends IBasePageService {

    /**
     * 从空白新增大屏页
     *
     * @param bigScreenPageDTO
     * @return
     */
    String add(DataRoomPageDTO bigScreenPageDTO);


    /**
     * 从模板新增大屏页
     *
     * @param bigScreenPageDTO
     * @return
     */
    String addByTemplate(DataRoomPageDTO bigScreenPageDTO);

    /**
     * 根据编码获取大屏页配置
     *
     * @param bigScreenPageDTO
     * @return
     */
    DataRoomPageDTO getConfigByTemplate(DataRoomPageDTO bigScreenPageDTO);

    /**
     * 分页查询
     *
     * @param searchDTO
     * @return
     */
    PageVO<PageEntity> getByCategory(DataRoomSearchDTO searchDTO);

    /**
     * 更新大屏页
     *
     * @param bigScreenPageDTO
     */
    void update(DataRoomPageDTO bigScreenPageDTO);

    /**
     * 复制大屏页
     * @param screenEntity
     * @return
     */
    String copy(PageEntity screenEntity);

    /**
     * 根据编码删除
     *
     * @param code
     */
    void deleteByCode(String code);

}
