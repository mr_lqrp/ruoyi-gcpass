package com.ruoyi.dataset.service.impl.dataset;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.dataset.common.exception.GlobalException;
import com.ruoyi.dataset.common.utils.GroovyUtils;
import com.ruoyi.dataset.constant.DatasetConstant;
import com.ruoyi.dataset.dao.DatasetDao;
import com.ruoyi.dataset.dto.DatasetParamDTO;
import com.ruoyi.dataset.dto.TestExecuteDTO;
import com.ruoyi.dataset.entity.DatasetEntity;
import com.ruoyi.dataset.entity.config.GroovyDataSetConfig;
import com.ruoyi.dataset.params.ParamsClient;
import com.ruoyi.dataset.permission.DatasetPermissionClient;
import com.ruoyi.dataset.service.IBaseDataSetService;
import com.ruoyi.dataset.vo.DataVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author hongyang
 * @version 1.0
 * @date 2023/6/1 11:20
 */
@Slf4j
@Service(DatasetConstant.DataSetType.SCRIPT)
public class GroovyDataSetServiceImpl extends ServiceImpl<DatasetDao, DatasetEntity> implements IBaseDataSetService {

    @Resource
    private ParamsClient paramsClient;

    @Resource
    private DatasetPermissionClient datasetPermissionClient;


    @Override
    public String add(DatasetEntity entity) {
        String id = IBaseDataSetService.super.add(entity);
        if (datasetPermissionClient.hasPermissionService()) {
            // 添加数据集权限
            datasetPermissionClient.addPermission(id);
        }
        return id;
    }

    @Override
    public void delete(String id) {
        IBaseDataSetService.super.delete(id);
        if (datasetPermissionClient.hasPermissionService()) {
            // 删除数据集权限
            datasetPermissionClient.deletePermission(id);
        }
    }

    @Override
    public Object execute(String id, List<DatasetParamDTO> params) {
        if (StringUtils.isBlank(id)) {
            throw new GlobalException("数据集id不能为空");
        }
        final List<DatasetParamDTO> finalParams = params;
        DatasetEntity datasetEntity = this.getByIdFromCache(id);
        if (datasetEntity == null) {
            throw new GlobalException("数据集不存在");
        }
        if (DatasetConstant.DatasetCache.OPEN.equals(datasetEntity.getCache())) {
            CompletableFuture<Object> future = DATASET_CACHE.get(id, key -> getData(finalParams, datasetEntity));
            try {
                return future.get();
            } catch (Exception e) {
                log.error("数据集缓存异常：{}", e.getMessage());
                log.error(ExceptionUtils.getStackTrace(e));
            }
        }
        return getData(finalParams, datasetEntity);

    }

    /**
     * 获取数据
     * @param finalParams
     * @param datasetEntity
     * @return
     */
    private Object getData(List<DatasetParamDTO> finalParams, DatasetEntity datasetEntity) {
        GroovyDataSetConfig config = (GroovyDataSetConfig) datasetEntity.getConfig();
        String script = config.getScript();
        // 参数预处理
        List<DatasetParamDTO> paramList = paramsClient.handleParams(finalParams);
        Map<String, Object> paramMap = this.buildParams(paramList, script);
        return GroovyUtils.run(script, paramMap);
    }


    @Override
    public DataVO execute(TestExecuteDTO executeDTO) {
        String script = executeDTO.getScript();
        if (StringUtils.isBlank(script)) {
            throw new GlobalException("脚本不能为空");
        }
        List<DatasetParamDTO> params = executeDTO.getParams();
        // 参数预处理
        params = paramsClient.handleParams(params);
        Map<String, Object> paramMap = this.buildParams(params, script);
        DataVO dataVO = new DataVO();
        dataVO.setData(GroovyUtils.run(script, paramMap));
        return dataVO;
    }


    /**
     * 构建参数，并且编译脚本
     * @param params
     * @param script
     * @return
     */
    private Map<String, Object> buildParams(List<DatasetParamDTO> params, String script) {
        Map<String, Object> paramMap = new HashMap<>(16);
        if (!CollectionUtils.isEmpty(params)) {
            params.forEach(p -> paramMap.put(p.getName(), p.getValue()));
        }
        Class clazz = GroovyUtils.buildClass(script);
        if (clazz == null) {
            throw new GlobalException("脚本编译异常");
        }
        return paramMap;
    }
}
