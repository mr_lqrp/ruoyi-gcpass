package com.ruoyi.dataset.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.dataset.entity.LabelEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hongyang
 * @version 1.0
 * @date 2023/7/5 10:30
 */
@Mapper
public interface LabelDao extends BaseMapper<LabelEntity> {


}
