package com.ruoyi.dataset.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.dataset.entity.DatasourceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhang.zeJun
 * @date 2022-11-14-11:41
 */
@Mapper
public interface DatasourceDao extends BaseMapper<DatasourceEntity> {
    

}
